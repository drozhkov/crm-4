const mysql = require('mysql2/promise');
const dbConfig = require('../config/db.json');

class Db {
  static async setup() {
    const { database, host, user, password } = dbConfig;

    const pool = await mysql.createPool({
      database,
      host,
      user,
      password,
      connectionLimit: 10
    });

    const connection = await pool.getConnection();

    try {
      const db = new this(connection);
      return db;
    } catch (error) {
      throw new Error(`--- There's an issue with the db connection. ---`);
    } finally {
      connection.release();
    }
  }

  constructor(connection) {
    this.connection = connection;
  }

  async get(query, ...params) {
    const q = await this.normalizeQuery(query);
    return await this.connection.execute(q, params);
  }

  async format(query) {}

  async normalizeQuery(str) {
    return str.replace(/\$\s*[0-9]+/g, '?');
  }
}
module.exports = Db;
