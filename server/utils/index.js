const cloneDeep = require('lodash').cloneDeep;

function prepareEmployeesData(employees) {
  return employees.map(employee => {
    const preparedEmployee = cloneDeep(employee);
    preparedEmployee.departments = preparedEmployee.departments.split(',').map(dep => +dep);
    return preparedEmployee;
  });
}
exports.prepareEmployeesData = prepareEmployeesData;

function prepareDepartmentsData(departments) {
  const output = departments.map(el => cloneDeep(el));

  return output
    .map(el => {
      el.children = output.filter(e => e.ancestor === el.id);
      el.hasDescendants = Boolean(el.hasDescendants);
      return el;
    })
    .filter(el => !el.ancestor);
}
exports.prepareDepartmentsData = prepareDepartmentsData;
