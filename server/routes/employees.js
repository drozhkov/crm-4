const express = require('express');
const getEmployee = require('../queries/employees').getEmployee;

const router = express.Router();

router.get('/:id?', async (req, res) => {
  const id = req.query.id;
  const [employee] = await getEmployee(id);

  res.json(employee);
});

exports.default = router;
