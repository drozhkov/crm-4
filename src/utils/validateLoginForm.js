import { FIELD_IS_REQUIRED_MSG, INVALID_EMAIL_MSG } from '../constants';

export default function validateLoginForm(values) {
  const errors = {};
  if (!values.email) {
    errors.email = FIELD_IS_REQUIRED_MSG;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = INVALID_EMAIL_MSG;
  }
  if (!values.password) {
    errors.password = FIELD_IS_REQUIRED_MSG;
  }
  return errors;
}
