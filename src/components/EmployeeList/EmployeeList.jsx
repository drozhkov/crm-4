import React from 'react';
import PropTypes from 'prop-types';
import style from './EmployeeList.css';
import EmployeeLink from '../EmployeeLink/EmployeeLink';
import sortAndFilterEmployeeList from '../../utils/sortAndFilterEmployeeList';

const EmployeeList = ({
  employees, sortBy, filterByDepartment, filterByVacation, searchTerm
}) => (
  <div className={style.employees}>
    <div className={style.employees__header}>
      <span>Name</span>
      <span>Position</span>
      <span>Department</span>
    </div>
    <ul className={style.employees__list}>
      {sortAndFilterEmployeeList(employees, {
        sortBy,
        filterByDepartment,
        filterByVacation,
        searchTerm
      }).map(employee => (
        <li key={employee.id} className={style.employees__item}>
          <EmployeeLink
            id={employee.id}
            name={`${employee.firstName} ${employee.secondName}`}
            position={employee.position}
            departmentTitle={employee.departmentTitle}
          />
        </li>
      ))}
    </ul>
  </div>
);

EmployeeList.propTypes = {
  employees: PropTypes.arrayOf(PropTypes.object).isRequired,
  sortBy: PropTypes.string.isRequired,
  filterByDepartment: PropTypes.number.isRequired,
  filterByVacation: PropTypes.bool.isRequired,
  searchTerm: PropTypes.string.isRequired
};

export default EmployeeList;
