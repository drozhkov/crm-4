import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import style from './Input.css';

const Input = ({ field, form: { errors }, ...props }) => {
  const inputClasses = cx({
    [style.input]: true,
    [style.input_has_error]: errors[field.name]
  });

  return (
    <div className={inputClasses}>
      <input {...field} {...props} className={style.input__el} />
      {errors[field.name] && <div className={style.input__error}>{errors[field.name]}</div>}
    </div>
  );
};

export default Input;
