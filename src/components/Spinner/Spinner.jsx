import React from 'react';
import style from './Spinner.css';
import imgSrc from '../../static/spinner.svg';

const Spinner = () => (
  <div className={style.spinner}>
    <img src={imgSrc} alt="Loading..." />
  </div>
);

export default Spinner;
